package dao;

import entity.Department;

public interface DepartmentDAO {
	
	public void insertDepartment(Department department);
	public Department getDepartment(int department_number);

}
