package dao;

public interface DAOFactory {
	
	public PersonDAO getPersonDAO();
	public DepartmentDAO getDepartmentDAO();
	

}
