package dao;

import entity.Person;

public class PersonDAOImp implements PersonDAO{

	@Override
	public void insertPerson(Person person) {
		
		//This is just a show-case do your insert SQL operation here.
		
		System.out.println("Person inserted to DB: \nSSN : " + String.valueOf(person.getSSN())
				+ "\nNAME : " +  person.getName() + "\nSURNAME : " + person.getSurname() + "\n\n");
	}

	@Override
	public Person getPerson(int SSN) {

		//This is just a show-case do your get SQL operation here.
		
		//This is just an random person.
		//In normal case, SSN will be used to fetch from DB.
		
		return new Person(123456789, "mert", "simsek");
	}

}
