package dao;

public class DAOFactoryImp implements DAOFactory{

	@Override
	public PersonDAO getPersonDAO() {
		return new PersonDAOImp();
	}

	@Override
	public DepartmentDAO getDepartmentDAO() {
		return new DepartmentDAOImp();
	}

}
