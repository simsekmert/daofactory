package dao;

import entity.Person;

public interface PersonDAO {

	public void insertPerson(Person person);
	public Person getPerson(int SSN);
}
