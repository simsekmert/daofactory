package dao;

import entity.Department;

public class DepartmentDAOImp implements DepartmentDAO{

	@Override
	public void insertDepartment(Department department) {

		System.out.println("Department inserted: \nDEPARTMENT NUMBER : " + String.valueOf(department.getDepartment_number())
				+ "\nDEPARTMENT NAME : " +  department.getDepartment_name() + "\nDEPARTMENT LOCATION : " + department.getDepartment_location() + "\n\n");
		
	}

	@Override
	public Department getDepartment(int department_number) {

		//This is just an random department.
		//In normal case, department_number will be used to fetch from DB.
		
		return new Department(1, "IT Department", "Turkey");
	}

}
