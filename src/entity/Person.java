package entity;

public class Person {
	
	int SSN;
	String name;
	String surname;
	
	public Person(){}
	
	public Person(int SSN, String name, String surname){
		this.SSN = SSN;
		this.name = name;
		this.surname = surname;
	}
	
	public int getSSN() {
		return SSN;
	}
	public void setSSN(int sSN) {
		SSN = sSN;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

}
