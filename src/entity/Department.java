package entity;

public class Department {
	
	int department_number;
	String department_name;
	String department_location;
	
	public Department(){}
	
	public Department(int department_number, String department_name, String department_location){
		this.department_number = department_number;
		this.department_name = department_name;
		this.department_location = department_location;
	}
	
	public int getDepartment_number() {
		return department_number;
	}
	public void setDepartment_number(int department_number) {
		this.department_number = department_number;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public String getDepartment_location() {
		return department_location;
	}
	public void setDepartment_location(String department_location) {
		this.department_location = department_location;
	}
	
}
