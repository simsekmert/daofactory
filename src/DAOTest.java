import dao.DAOFactory;
import dao.DAOFactoryImp;
import entity.Department;
import entity.Person;


public class DAOTest {
	public static void main(String[] args) {
		
		DAOFactory factory = new DAOFactoryImp();
		
		//Create new person and insert it to DB
		factory.getPersonDAO().insertPerson(new Person(123456789, "mert", "simsek"));
		//Create new department and insert it to DB
		factory.getDepartmentDAO().insertDepartment(new Department(1, "IT Department", "Turkey"));
		
		//Fetch department from DB and print values
		Department department = factory.getDepartmentDAO().getDepartment(1);
		
		System.out.println("Department fetched from DB: \nDEPARTMENT NUMBER : " + String.valueOf(department.getDepartment_number())
				+ "\nDEPARTMENT NAME : " +  department.getDepartment_name() + "\nDEPARTMENT LOCATION : " + department.getDepartment_location());
		
		System.out.println("\n\n");
		
		//Fetch person from DB and print values
		Person person = factory.getPersonDAO().getPerson(123456789);
		
		System.out.println("Person fetched from DB: \nSSN : " + String.valueOf(person.getSSN())
				+ "\nNAME : " +  person.getName() + "\nSURNAME : " + person.getSurname());

		System.out.println("\n\n");
		
	}

}
